%define debug_package %{nil}

Name:           terraform
Version:        0.11.14
Release:        1%{?dist}
Summary:        Write, Plan, and Create Infrastructure as Code.
Group:          Applications/System
License:        MPLv2.0
URL:            https://terraform.io/
Source0:        https://github.com/hashicorp/terraform/archive/v%{version}.tar.gz
BuildRequires:  golang >= 1.11
BuildRequires:  make which zip
ExclusiveArch: x86_64 i686 i586 i386

%description
Terraform enables you to safely and predictably create, change, and improve
production infrastructure. It is an open source tool that codifies APIs into
declarative configuration files that can be shared amongst team members, treated
as code, edited, reviewed, and versioned.

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export PATH=${PATH}:${GOPATH}/bin

%ifarch x86_64 amd64
  export XC_ARCH=amd64
%else
  export XC_ARCH=386
%endif

export XC_OS=linux
mkdir -p src/github.com/hashicorp/terraform
shopt -s extglob dotglob
mv !(src) src/github.com/hashicorp/terraform
shopt -u extglob dotglob
pushd src/github.com/hashicorp/terraform
make tools && make bin
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 src/github.com/hashicorp/terraform/bin/%{name} $RPM_BUILD_ROOT%{_bindir}

%clean
rm -rf %{buildroot}

%files
%license src/github.com/hashicorp/terraform/LICENSE
%doc src/github.com/hashicorp/terraform/{BUILDING.md,CHANGELOG.md,README.md}
%{_bindir}/%{name}

%changelog
